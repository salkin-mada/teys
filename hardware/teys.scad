include</home/salkin/Kode/GIT/openscad.nvim/lua/openscad/modules/roundedcube.scad>
$fs=0.10;
width=27; depth=13.5; height=3.5; corner_radius=0.5; wall_size=0.30;
knob_size=0.5; led_size=0.254; led_slant=0.3;
m=0.01; // cutout extra
open = true;
// open = false;
lift = 2;
// show_keys=true;
show_keys=false;
num_white_keys=10;

white_keys_width=23.2;
white_keys_depth=2.6;
black_keys_width=1.1;
black_keys_depth=2.8;
module white_keys() {
    // scale on z axis to make sure to cut out downwards
    color("white") translate([0,-depth/2+white_keys_depth/2+wall_size,height/2-wall_size/2/2-m])
        scale([1,1,10]) cube([white_keys_width,white_keys_depth,wall_size/2+m+m],true);
}
module black_keys() {
    for (n=[1:num_white_keys]) {
        if (n!=3&&n!=7&&n!=10) { // no black keys positions
            echo(n);
            color("black") translate([
                    -white_keys_width/2+((white_keys_width/10))*n,
                    -depth/2+black_keys_depth/2+wall_size+white_keys_depth-m,
                    height/2-wall_size/2/2-m
            ])
                scale([1,1,10]) cube([black_keys_width,black_keys_depth+(m*2),wall_size/2+m+m],true);

        }

    }
}

module top() { 
    difference() {
        roundedcube([width,depth,height], true, corner_radius, "all");
        roundedcube([width-wall_size,depth-wall_size,height-wall_size], true, corner_radius, "all");
        translate([0,0,-height/2]) cube([width+m,depth+m,height],true);
        //cutouts
        //knob
        translate([-width/2+2,-depth/2+white_keys_depth+2,height/2-(wall_size/2)-m]) cylinder(wall_size/2+m,knob_size,knob_size); // knob hole
        // led
        translate([width/2-1.5,depth/2-1.5,height/2-(wall_size/2)]) {
            translate([0,0,-m])cylinder(wall_size/2/2+m+m,led_size,led_size); // led hole
            translate([0,0,wall_size/2/2])cylinder(wall_size/2/2,led_size,led_size+led_slant); // led slant
        }
        white_keys();
        black_keys();
    }
    if (show_keys) {
        difference() {
            white_keys();
            for(n=[1:num_white_keys]) {
                color("orange") translate([
                    -white_keys_width/2+((white_keys_width/10))*n,
                -depth/2+wall_size,
                (height/2-wall_size/2/2-m)+0.7
                ])
                scale([0.1,white_keys_depth,1]) square();
            }
        }
        black_keys();
    }
}


module bund() { 
    difference() {
        roundedcube([width,depth,height], true, corner_radius, "all");
        roundedcube([width-wall_size,depth-wall_size,height-wall_size], true, corner_radius, "all");
        translate([0,0,height/2]) cube([width+m,depth+m,height],true);
    }
}

// draw
// top
if (open) rotate([2,0,0]) translate([0,0,lift]) top(); else top();
// bund
if (open) rotate([20,0,0]) translate([0,depth,-lift]) bund(); else bund();

// cut-outs
// scale([1,1,0.2]) square([l+0.1,w+0.1], true);

// cube([23,23,23],true);

// difference(){
// cylinder(h=2,d=4,$fn=48,center=true);
// cylinder(h=3,d=1.5,$fn=48,center=true);
// translate([0,2,0]){
// cube([5,2,3],center=true);//right side
// translate([0,-4,0]){
// cube([5,2,3],center=true);//left side
// }}}

