#ifndef SATANGLOBALS
#define SATANGLOBALS
#pragma once

#include <Arduino.h>

namespace teys {
enum class MIDIMODE { _7BIT, _14BIT };

constexpr bool DEBUG = false;
constexpr bool MIDI_DEBUG = false;

int channel = 1;

int octave = 4;

constexpr int num_buttons = 18;
constexpr int num_special_buttons = 1;

constexpr int pitches_per_octave = 12;

inline int clipValue(int value, int minValue, int maxValue) {
    if (value > maxValue) {
        return maxValue;
    } else if (value < minValue) {
        return minValue;
    } else {
        return value;
    }
}

inline int calculate_midi_note(int button_num, int num_buttons, int octave) {
    auto val = button_num + (octave * pitches_per_octave);
    auto midi_note = clipValue(val, 0, 127);
    return midi_note;
}

inline String gen_random(const int len) {
    String tmp_s;
    static const char alphanum[] = "(/\\)";
    for (auto i = 0; i < len; ++i) 
    	tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    return tmp_s;
}

} // namespace teys
#endif
