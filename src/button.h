#ifndef SATANBUTTON
#define SATANBUTTON
#pragma once

#include <Arduino.h>
#include <Bounce2.h>
#include "globals.h"

namespace teys {

enum class ButtonAction {
    Nothing,
    FuncSwitch,
    SendNoteOn,
    SendNoteOff,
    Print
};

/*
* @brief Button for Teys
* @author Niklas Adam
* @date 23-07-2020
*/
class Button {
public:
int pin;
ButtonAction onAction, offAction;
int buttonFunc;

Bounce button = Bounce();
int buttonNumber;

Button(int initPin, int button_num)
: button(),
buttonNumber(button_num) {
    pin = initPin;
};

void setup(ButtonAction buttonOnAction, ButtonAction buttonOffAction, int debounce_time) {
    onAction = buttonOnAction;
    offAction = buttonOffAction;
    state = HIGH; // not pressed
    pinMode(pin, INPUT_PULLUP);
    button.attach(pin);
    button.interval(debounce_time);
};


elapsedMillis retrig_msec = 0;
elapsedMillis hold_msec = 0;
void read() {
    button.update();
    auto reading = button.read();
    if (reading != state) {
        if (reading == HIGH) {
            performOffAction();
        } else if (reading == LOW) {
            performOnAction();
        }
        state = reading;
    }
};

void setButtonOnAction(ButtonAction buttonAction) { onAction = buttonAction; }
void setButtonOffAction(ButtonAction buttonAction) { offAction = buttonAction; }

void print() {
    int midi_note = calculate_midi_note(buttonNumber, num_buttons-num_special_buttons, octave);
    Serial.print("Button num ");
    Serial.print(buttonNumber);
    Serial.print(" on pin ");
    Serial.print(pin);
    Serial.print(": ");
    Serial.print(state);
    Serial.print("\n\tmidi note: ");
    Serial.println(midi_note);
}

private:
int state;
int midi_note;
int last_midi_note_on_number;

void performAction(ButtonAction doit) {
    switch (doit) {
        case ButtonAction::Nothing:
            break;
        case ButtonAction::FuncSwitch:
            /* func_button = PRESSED */
            break;
        case ButtonAction::SendNoteOn:
            if(MIDI_DEBUG) {
                Serial.print("btn ");
                Serial.print(buttonNumber);
                Serial.print(" on: ");
                Serial.println(calculate_midi_note(buttonNumber, num_buttons-num_special_buttons, octave));
            }

            midi_note = calculate_midi_note(buttonNumber, num_buttons-num_special_buttons, octave);
            usbMIDI.sendNoteOn(midi_note, 100, channel);
            last_midi_note_on_number = midi_note;
            break;
        case ButtonAction::SendNoteOff:
            if(MIDI_DEBUG) {
                Serial.print("btn ");
                Serial.print(buttonNumber);
                Serial.print(" off: ");
            }
            midi_note = calculate_midi_note(buttonNumber, num_buttons-num_special_buttons, octave);
            if(midi_note != last_midi_note_on_number) {
                midi_note = last_midi_note_on_number;
                if (MIDI_DEBUG) Serial.print("[recall release] -->");
            }
            if(MIDI_DEBUG) Serial.println(midi_note);
            usbMIDI.sendNoteOff(midi_note, 100, channel);
            break;
        case ButtonAction::Print:
            print();
            break;
    }
}

void performOnAction() { performAction(onAction); }

void performOffAction() { performAction(offAction); }
};

} // namespace teys

#endif
