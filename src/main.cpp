#include <Arduino.h>
#include "button.h"
#include <array>
#include "midi.h"
#include "globals.h"

#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>

using namespace teys;

Midi midi(MIDIMODE::_7BIT);

constexpr int min_val = 0;
constexpr int _7bit_max = 127;
constexpr int _14bit_max = 16383;
int midi_mode_max;

int led_pin = 13;

// Initialize encoder
Encoder octave_encoder(30, 29);

// Initialize buttons
std::array<Button, num_buttons> buttons{
Button(0, 0),
Button(1, 1),
Button(2, 2),
Button(3, 3),
Button(4, 4),
Button(5, 5),
Button(6, 6),
Button(7, 7),
Button(8, 8),
Button(9, 9),
Button(10, 10),
Button(11, 11),
Button(12, 12),
Button(24, 13),
Button(25, 14),
Button(26, 15),
Button(27, 16),
Button(28, 17)
};

constexpr int debounce = 10; // ms

void midi_buttons_setup() {
    for (size_t b = 0; b < (buttons.size()-num_special_buttons); b++) {
        buttons[b].setup(ButtonAction::SendNoteOn, ButtonAction::SendNoteOff, debounce);
    }
    buttons[17].setup(ButtonAction::Nothing, ButtonAction::Nothing, debounce);
}

void setup() {
    pinMode(led_pin, OUTPUT);
    if (MIDI_DEBUG) {
        while (!Serial);
        for(auto i = 0; i < 5; i++) {
            digitalWrite(led_pin, HIGH);
            delay(35);
            digitalWrite(led_pin, LOW);
            delay(35);
        }
        Serial.println("MIDI DEBUG MODE ENABLED");
    }
    if (DEBUG) {
        while (!Serial);
        for(auto i = 0; i < 10; i++) {
            digitalWrite(led_pin, HIGH);
            delay(35);
            digitalWrite(led_pin, LOW);
            delay(35);
        }
        Serial.println("OVERALL DEBUG MODE ENABLED");
        for (size_t b = 0; b < buttons.size(); b++) {
            Serial.print(b);
            buttons[b].setup(ButtonAction::Print, ButtonAction::Nothing, debounce);
        }
        Serial.println();
    } else {
        midi_buttons_setup();
    }
    for(auto i = 0; i < 5; i++) {
        digitalWrite(led_pin, HIGH);
        delay(100);
        digitalWrite(led_pin, LOW);
        delay(100);
    }
    octave_encoder.write(octave*4);
}

void readButtons() {
    for (size_t btn = 0; btn < buttons.size(); btn++) {
        buttons[btn].read();
    }
}

void readEncoder() {
    int newOctave;
    long val = octave_encoder.read();
    if (DEBUG) Serial.print("encoder value: "); Serial.println(val);
    newOctave = (val/4) % 8; // octave: 0-7
    if (DEBUG) Serial.print("new octave: "); Serial.println(newOctave);
    if (newOctave != octave) {
        octave = newOctave;
    }
    /* octave_encoder.write(octave*4); */
}

void loop() {
    readEncoder();
    readButtons();
    midi.discardIncomingMIDI();
    delay(10);
    if (DEBUG || MIDI_DEBUG) {
       delay(20);
    }
}
